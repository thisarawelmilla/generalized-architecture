from concurrent.futures import ThreadPoolExecutor
import python.queue_manager.video as video
import python.queue_manager.image as image


# initiate the queue manager
def initiate_queue_manager():
    global executor
    executor = ThreadPoolExecutor(1)


# make a queue depend on the data type
def initiate_queue(param):
    if param.data_type == "image":
        new_queue = image.QueueObject(param)
        executor.submit(image.check_queue, new_queue)
    else:
        new_queue = video.QueueObject(param)
        executor.submit(video.check_queue, new_queue)
    return new_queue
