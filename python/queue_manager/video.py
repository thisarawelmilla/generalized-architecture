import queue as Queue
import os
import cv2
import exiftool


class QueueObject(object):
    def __init__(self, param):
        self.dir_path = param.dir_path
        self.save_path = param.save_path
        self.processed_image_path = param.processed_image_path
        self.data_type = param.data_type
        self.is_save_result = param.is_save_result
        self.is_save_original = param.is_save_original
        self.speed = param.speed
        self.input_queue = Queue.Queue(3)
        self.output_queue = Queue.Queue(2)
        self.lock = False
        self.video_list = os.listdir(self.dir_path)
        self.img_in_queue = []
        self.meta_data_list = []

    def get_queue(self):
        return self.input_queue

    def get_output_queue(self):
        return self.output_queue

    def add_videos(self):
        video_list = os.listdir(self.dir_path)
        self.video_list = video_list

    def read_meta_data(self, video_path):
        with exiftool.ExifTool() as et:
            metadata = et.get_metadata(video_path)

        cam_id = metadata['XMP:Creator']
        self.meta_data_list.append(cam_id)


def initiate_video(video_path):
    cap = cv2.VideoCapture(video_path)
    total_frames = cap.get(7)
    return cap


def read_frame(cap, frame_num):
    cap.set(1, frame_num)
    ret, frame = cap.read()
    if ret:
        return frame
    else:
        return None


def initiate_writer(q, frame_rate, size):
    global out
    out = cv2.VideoWriter(q.save_path + "/" + "tes_video.avi", cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), frame_rate,
                          size)


def write_frame(frame, size):
    frame = cv2.resize(frame, size)
    out.write(frame)


def release_video():
    out.release()


def check_queue(q):
    while True:
        if len(q.video_list) == 0:
            q.add_videos()
        if len(q.video_list) > 0:
            video_path = q.dir_path + "/" + q.video_list[0]
            q.read_meta_data(video_path)
            cap = initiate_video(video_path)
            size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),)
            initiate_writer(q, 1, size)
            del q.video_list[0]
            frame_num = 0
            while True:
                if q.get_queue().qsize() < 3:
                    frame = read_frame(cap, frame_num)
                    frame_num += q.speed
                    if frame is not None:
                        q.get_queue().put(frame)
                        image_name = "frame" + "/" + str(frame_num)
                        q.img_in_queue.append(image_name)
                    else:
                        cap.release()
                if q.output_queue.qsize() > 0:
                    if len(q.img_in_queue) > 0:
                        write_frame(q.output_queue.get(), size)
                    else:
                        release_video()
                        os.remove(video_path)
                        break
