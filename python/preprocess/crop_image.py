def crop(img, crop_coord):
    crop_img_list = []
    for coord in crop_coord:
        crop_img = img[coord[1]:coord[3], coord[0]:coord[2]]
        crop_img_list.append(crop_img)
    return crop_img_list


