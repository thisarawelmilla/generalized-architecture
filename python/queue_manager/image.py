import queue as Queue
import time
import os
import cv2
from iptcinfo3 import IPTCInfo as iptc_info


class QueueObject(object):
    def __init__(self, param):
        self.dir_path = param.dir_path
        self.save_path = param.save_path
        self.processed_image_path = param.processed_image_path
        self.data_type = param.data_type
        self.is_save_result = param.is_save_result
        self.is_save_original = param.is_save_original
        self.input_queue = Queue.Queue(3)
        self.lock = False
        self.img_list = os.listdir(self.dir_path)
        self.img_in_queue = []
        self.meta_data_list = []

    def get_datatype(self):
        return self.data_type

    def get_queue(self):
        return self.input_queue

    def get_lock(self):
        return self.lock

    def set_lock(self, status):
        self.lock = status

    def add_images(self):
        img_list = os.listdir(self.dir_path)
        self.img_list = img_list

    def read_meta_data(self, image_name):
        info = iptc_info(self.dir_path + "/" + image_name)
        cam_id = info['content location code']
        self.meta_data_list.append(cam_id)


def image_queue(q):
    if len(q.img_list) == 0:
        q.add_images()
    while not q.get_lock() and len(q.img_list) > 0:
        q.set_lock(True)
        image_name = q.img_list[0]
        image = cv2.imread(q.dir_path + "/" + image_name)
        q.read_meta_data(image_name)
        del q.img_list[0]
        os.remove(q.dir_path + "/" + image_name)
        q.get_queue().put(image)
        q.img_in_queue.append(image_name)
        q.set_lock(False)
        break


def check_queue(q):
    while True:
        if q.get_queue().qsize() < 3:
            if q.get_datatype() == "image":
                image_queue(q)
        if q.get_queue().qsize() >= 3:
            time.sleep(1)
