import python.controller.validation as validate_config
import python.detector.detector as detector
import python.postprocess.view_results as view_result
import python.postprocess.save_image as save_image
import python.queue_manager.queue_manager as queue_manager
import python.queue_manager.video as video
import python.preprocess.meta_data_read as meta_data_read
import python.preprocess.crop_image as crop_image
import python.postprocess.line_cut as cut
import logging

config_path = '/home/thisara/Music/generalized_machine_learning_architecture/python/configuration/config.json'
schema_file_path = '/home/thisara/Music/generalized_machine_learning_architecture/python/controller/jsonschema_file.json'

queue_manager_param, detection_param = validate_config.validation(config_path, schema_file_path)
metadata = meta_data_read.read_data(queue_manager_param.meta_data)
queue_manager.initiate_queue_manager()
queue_obj = queue_manager.initiate_queue(queue_manager_param)
detector.initiate_framework(detection_param)

while True:
    # get the images from the queue and pass it detctor
    if not queue_obj.input_queue.empty():
        image = queue_obj.input_queue.get()
        image_name = queue_obj.img_in_queue[0]
        coordinate, side, crop = meta_data_read.get_data(queue_obj.meta_data_list[0], metadata)
        cropped_image = [image]
        # cropped_image = crop_image.crop(image, crop)
        for img in cropped_image:
            detection = detector.detection(detection_param, img)
            count_list, box_list = cut.get_count(detection["0"][0], coordinate, side)
            print(count_list)
            logging.info(count_list)
            # view_result.view(img, detection, box_list)
            queue_obj.output_queue.put(img)
        del queue_obj.img_in_queue[0]
        # save_image.save(image, image_name, queue_obj.processed_image_path)
