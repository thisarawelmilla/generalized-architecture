import numpy as np
import cv2
import os


class yolo_detection_net:
    def __init__(self, model_path):
        self.weights_path = os.path.sep.join([model_path, "yolov3.weights"])
        self.config_path = os.path.sep.join([model_path, "yolov3.cfg"])
        self.graph = cv2.dnn.readNetFromDarknet(self.config_path, self.weights_path)

    def process_frame(self, image):
        ln = self.graph.getLayerNames()
        ln = [ln[i[0] - 1] for i in self.graph.getUnconnectedOutLayers()]
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
                                     swapRB=True, crop=False)
        self.graph.setInput(blob)
        layer_outputs = self.graph.forward(ln)
        return layer_outputs


def initiate_model(model_path):
    global net
    net = yolo_detection_net(model_path=model_path)


def detector(threshold, confidence, img, class_list="all_classes"):
    (im_height, im_width) = img.shape[:2]
    layer_outputs = net.process_frame(img)

    boxes = []
    confidences = []
    classes = []
    for output in layer_outputs:
        for detection in output:
            scores = detection[5:]
            class_inst = np.argmax(scores)
            confidence_inst = scores[class_inst]
            if confidence_inst > threshold and class_inst in class_list:
                box = detection[0:4] * np.array([im_width, im_height, im_width, im_height])
                (centerX, centerY, width, height) = box.astype("int")
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence_inst))
                classes.append(class_inst)
    detection_result = cv2.dnn.NMSBoxes(boxes, confidences, threshold, confidence)

    detection = {}
    for class_inst in class_list:
        detection[str(class_inst)] = ([], 0)
    if len(detection_result) > 0:
        for i in detection_result.flatten():
            class_index = str(np.int16(classes[i]).item())
            detection[class_index][0].append((int(boxes[i][1]),
                                              int(boxes[i][0]),
                                              (int(boxes[i][1] + boxes[i][3])),
                                              (int(boxes[i][0] + boxes[i][2]))))
    return detection
