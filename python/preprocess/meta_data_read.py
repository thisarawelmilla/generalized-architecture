import ast
import json


# read the metadata json file
def read_data(data_path):
    with open(data_path) as config_file:
        config_data = json.load(config_file)
    return config_data


# assign value for required parameters
def get_data(cam_id, meta_data):
    if type(cam_id) == bytes:
        cam_id = cam_id.decode("utf-8")

    cam_id = str(cam_id)
    coordinates = ast.literal_eval(meta_data[cam_id]["coordinates"])
    side = meta_data[cam_id]["side"]
    crop = ast.literal_eval(meta_data[cam_id]["crop"])

    return coordinates, side, crop
