import jsonschema
import json
import os
import logging
import sys


# create detection object from detection parameters in config file
class detection(object):
    def __init__(self, detection_config):
        self.model_path = detection_config["model_path"]
        self.threshold = detection_config["threshold"]
        self.classes_list = detection_config["classes_list"]
        self.framework = detection_config["framework"]
        self.confidence = detection_config["confidence"]


# create queue manager object from detection parameters in config file
class queue_manager(object):
    def __init__(self, queue_manager_config):
        self.dir_path = queue_manager_config["dir_path"]
        self.save_path = queue_manager_config["save_result_dir"]
        self.meta_data = queue_manager_config["metadata_path"]
        self.speed = queue_manager_config["speed"]
        self.data_type = queue_manager_config["data_type"]
        self.processed_image_path = queue_manager_config["processed_data_dir"]
        self.is_save_result = queue_manager_config["save_results"]
        self.is_save_original = queue_manager_config["save_original"]


# check whether the paths given in config file are existing
def check_path_existence(config_data):
    if not os.path.exists(config_data["queue_manager"]["dir_path"]):
        logging.error("dir_path is not existing")
        sys.exit()
    try:
        if not os.path.exists(config_data["queue_manager"]["metadata_path"]):
            logging.error("dir_path is not existing")
            sys.exit()
    except ValueError:
        config_data["queue_manager"]["metadata_path"] = None
    try:
        if config_data["queue_manager"]["save_results"] and not os.path.exists(config_data["queue_manager"]
                                                                               ["save_result_dir"]):
            logging.error("dir_path is not existing")
            sys.exit()
    except ValueError:
        config_data["queue_manager"]["save_result_dir"] = None
    try:
        if config_data["queue_manager"]["save_original"] and not os.path.exists(config_data["queue_manager"]
                                                                                ["processed_data_dir"]):
            logging.error("dir_path is not existing")
            sys.exit()
    except ValueError:
        config_data["queue_manager"]["processed_dir"] = None
    if not os.path.exists(config_data["detector"]["model_path"]):
        logging.error("model_path is not existing")
        sys.exit()
    return config_data


# validate the config file against json schema and check path existent
def validation(json_file_path, schema_file_path):
    try:
        with open(json_file_path) as json_file:
            config_data = json.load(json_file)
        with open(schema_file_path) as schema_file:
            schema_file = json.load(schema_file)
        jsonschema.validate(instance=config_data, schema=schema_file)
    except jsonschema.exceptions.ValidationError as e:
        error = str(e).split("\n")[0]
        logging.error("missing data:" + error)
        sys.exit()
    except json.decoder.JSONDecodeError as e:
        error = str(e).split("\n")[0]
        logging.error("invalid json format:" + error)
        sys.exit()
    config_data = check_path_existence(config_data)

    queue_manager_object = queue_manager(config_data["queue_manager"])
    detection_object = detection(config_data["detector"])

    return queue_manager_object, detection_object
